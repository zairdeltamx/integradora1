<?php

namespace App\Http\Controllers;

use App\Http\Requests\Storetb_tarjetaRequest;
use App\Http\Requests\Updatetb_tarjetaRequest;
use App\Models\tb_tarjeta;

class TbTarjetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historial(){

        return tb_tarjeta::all();

    }
    public function sacar(){

        $data = tb_tarjeta::latest('id')->first();
        return $data;

    }
    public function index(){

        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/zairdeltamx/feeds/nfc/data/last?X-AIO-Key=aio_biTe50xUkfFJL4M8Xscp1haHaScs');
        $data=$respuesta->object();
            $model = new tb_tarjeta();
        
            $model->nombre_ingreso=$data->value;
           
            $model->save();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        tb_tarjeta::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'tarjeta guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(tb_tarjeta $tb_tarjeta)
    {
        return response()->json([
            'res'=>true,
            'tarjeta'=>$tb_tarjeta
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tb_tarjeta $tb_tarjeta)
    {
        $tb_tarjeta->update($tb_tarjeta->all());
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(tb_tarjeta $tb_tarjeta)
    {
        $tb_tarjeta->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'tarjeta eliminado'
        ],200);
    }
}
